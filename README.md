Mat's Config
============

## Installation :

### Using [Git][1] (*Recommended*)

I recommend using Git to install the config so that you can stay up to date if there are any changes.

1. Install Git if you haven't already.
2. Open Git Bash in your XVM configs folder.
3. `git clone git@git.matmunn.me:mjmunn/mats-config.git "Mat's Config"`

### Manual Install

1. Click the download zip button.
2. Open the downloaded zip file and extract the folder to your XVM configs folder.
3. Rename the directory something easy to remember.

---

After installing the files you'll need to enable the config.

1. Open xvm/configs/xvm.xc in your favorite text editor.
2. Add the line `${"Mat's Config/@xvm.xc":"."}`
3. Comment out any other lines with double forward slashes (`//`).

### Changes :

Feel free to create merge requests for any changes you make to the configuration.

 [1]: http://msysgit.github.io/